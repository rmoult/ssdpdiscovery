
Pod::Spec.new do |s|

    s.name         = "SSDPDiscovery"
    s.version      = "1.1.0"
    s.summary      = "SSDPDiscovery gives easy access to using M-SEARCH to discover devices"
    s.author       = "Richard Moult"
    s.homepage     = "https://rmoult@bitbucket.org/rmoult/ssdpdiscovery"
    s.license      = { :type => 'The MIT License (MIT)', :text => <<-LICENSE
                        Copyright (c) 2015 Richard Moult.
                        LICENSE
                        }
    s.source_files = 'SSDPDiscovery/SSDPClasses/Discovery/*.{h,m}'
    s.requires_arc = true
    s.platform     = :ios, '7.0'
    s.source       = {:git => "https://rmoult@bitbucket.org/rmoult/ssdpdiscovery.git", :tag=> '1.1.0'}
    s.dependency 'CocoaAsyncSocket', '~> 7.3.0'

end
