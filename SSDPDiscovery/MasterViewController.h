//
//  Created by Richard Moult on 09/01/2015.
//  Copyright (c) 2015 TrickySquirrel. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

@interface MasterViewController : UITableViewController

@property (strong, nonatomic) DetailViewController *detailViewController;


@end

