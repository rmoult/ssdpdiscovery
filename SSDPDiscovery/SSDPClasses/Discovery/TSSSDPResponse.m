//
//  Created by Richard Moult on 11/12/2014.
//  Copyright (c) 2014 TrickySquirrel. All rights reserved.
//

#import "TSSSDPResponse.h"


NSString *const SEARCHTARGETKEY = @"ST";
NSString *const LOCATIONKEY = @"LOCATION";
NSString *const SERVERKEY = @"SERVER";



@interface TSSSDPResponse()
@end



@implementation TSSSDPResponse


- (id)init {
    
    return [self initWithData:nil];
}


- (id)initWithData:(NSData *)data {
    
    NSAssert(data, @"data required");
    
    self = [super init];
    
    if (self) {
        
        NSString *message = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        
        _dictionary = [self dictionaryFromMessageString:message];
    }
    return self;
}


- (NSDictionary *)dictionaryFromMessageString:(NSString *)messageString {
    
    NSMutableDictionary *messageDictionary = [NSMutableDictionary dictionaryWithCapacity:10];
    
    NSArray *lines = [messageString componentsSeparatedByString:@"\r\n"];
    
    for (NSString *line in lines) {
        
        [self addKeyValueFromSSDPLine:line toDictionary:messageDictionary];
    }
    
    return messageDictionary;
}


- (void)addKeyValueFromSSDPLine:(NSString *)ssdpLine toDictionary:(NSMutableDictionary *)dictionary {
    
    NSRange keyRange = [ssdpLine rangeOfString:@":" options:NSCaseInsensitiveSearch];
    
    if (keyRange.location != NSNotFound) {
        
        NSString *key = nil, *value = nil;
        
        [self extractFromSSDPLine:ssdpLine withinKeyRange:keyRange key:&key value:&value];

        if (key && value) {
            
            [dictionary setValue:value forKey:key];
        }
    }
}


- (void)extractFromSSDPLine:(NSString *)line
             withinKeyRange:(NSRange)keyRange
                        key:(NSString **)key
                      value:(NSString **)value {
    
    NSString *keyString = [line substringToIndex:NSMaxRange(keyRange) - 1];
    
    NSString *valueString = [[line substringFromIndex:NSMaxRange(keyRange)] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

    if ((keyString && keyString.length) && (valueString && valueString.length)) {
        
        *key = keyString;
        *value = valueString;
    }
}


- (BOOL)doesDictionaryKey:(NSString *)key containString:(NSString *)string {
    
    NSString *keyValue = self.dictionary[key];
    
    if ([keyValue containsString:string]) {
        
        return YES;
    }
    
    return NO;
}

@end
