//
//  Created by Richard Moult on 11/12/2014.
//  Copyright (c) 2014 TrickySquirrel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TSAsyncUdpSocket.h"
#import "TSSSDPResponse.h"



typedef void (^TSSSDPDiscoverServiceBlock) (BOOL success, TSSSDPResponse *ssdpResponse, NSError *error);

@interface TSSSDPDiscovery : NSObject

- (id)initWithAsyncUpdSocket:(TSAsyncUdpSocket *)socket;

- (void)startSearchingWithSearchTarget:(NSString *)searchTarget responseTime:(NSInteger)responseTime completionBlock:(TSSSDPDiscoverServiceBlock)completion;

- (void)stopSearching;

@end
