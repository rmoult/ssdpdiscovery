//
//  Created by Richard Moult on 12/12/2014.
//  Copyright (c) 2014 TrickySquirrel. All rights reserved.
//

#import "TSAsyncUdpSocket.h"
#import "TSSSDPResponse.h"

NSString *const MULTICAST_ADDRESS = @"239.255.255.250";
NSInteger MULTICAST_PORT = 1900;


@interface TSAsyncUdpSocket () <GCDAsyncUdpSocketDelegate>
@end



@implementation TSAsyncUdpSocket


- (id)initWithQueue:(dispatch_queue_t)queue {
    
    self = [super initWithDelegate:self delegateQueue:queue];
    
    return self;
}


- (void)addSSDPSocketFilter:(GCDAsyncUdpSocketReceiveFilterBlock)filter queue:(dispatch_queue_t)queue {
    
    [self setReceiveFilter:filter withQueue:queue];
}


- (void)startBroadcastingWithData:(NSData *)data {
    
    NSError *error = nil;
    
    [self bindToPort:MULTICAST_PORT error:&error];
    [self joinMulticastGroup:MULTICAST_ADDRESS error:&error];
    [self enableBroadcast:YES error:&error];
    [self beginReceiving:&error];
    
    if (error) {
        [self.socketDelegate asyncUpdSocket:self didReceiveError:error];
    }
    
    [self sendData:data toHost:MULTICAST_ADDRESS port:MULTICAST_PORT withTimeout:-1 tag:1];
}




#pragma mark - delegate


- (void)udpSocket:(GCDAsyncUdpSocket *)sock didNotConnect:(NSError *)error {
    
    [self.socketDelegate asyncUpdSocket:self didReceiveError:error];
}


- (void)udpSocket:(GCDAsyncUdpSocket *)sock didReceiveData:(NSData *)data fromAddress:(NSData *)address withFilterContext:(id)filterContext {
    
    [self.socketDelegate asyncUdpSocket:self didReceiveData:data];
}


- (void)udpSocketDidClose:(GCDAsyncUdpSocket *)sock withError:(NSError *)error {
    
    if (error) {
        [self.socketDelegate asyncUpdSocket:self didReceiveError:error];
    }
}


@end
