//
//  Created by Richard Moult on 11/12/2014.
//  Copyright (c) 2014 TrickySquirrel. All rights reserved.
//

#import "TSSSDPDiscovery.h"
#import <CocoaAsyncSocket/GCDAsyncUdpSocket.h>



@interface TSSSDPDiscovery() <TSAsyncUdpSocketDelegate>
@property (nonatomic, strong) TSSSDPDiscoverServiceBlock completionBlock;
@property (nonatomic, strong) dispatch_queue_t searchQueue;
@property (nonatomic, strong) TSAsyncUdpSocket *discoverySocket;
@end



@implementation TSSSDPDiscovery


- (id)init {
    
    return [self initWithAsyncUpdSocket:nil];
}


- (id)initWithAsyncUpdSocket:(TSAsyncUdpSocket *)socket {
    
    NSAssert(socket, @"socket required");

    self = [super init];
    
    if (self) {
        
        _discoverySocket = socket;
        _discoverySocket.socketDelegate = self;
    }
    
    return self;
}


- (void)startSearchingWithSearchTarget:(NSString *)searchTarget responseTime:(NSInteger)responseTime completionBlock:(TSSSDPDiscoverServiceBlock)completion {

    NSAssert(completion, @"completion block required");
    NSAssert(searchTarget, @"search target required");
    
    self.completionBlock = completion;
    
    NSData *datagram = [self multicastSearchDatagramWithSearchTarget:searchTarget responseTime:responseTime];
    
    [self.discoverySocket startBroadcastingWithData:datagram];
}


- (NSData *)multicastSearchDatagramWithSearchTarget:(NSString *)searchTarget responseTime:(NSInteger)responseTime {
    
    NSString *datagramTemplate = @"M-SEARCH * HTTP/1.1\r\nHOST: %@:%d\r\nST: %@\r\nMAN: \"ssdp:discover\"\r\nMX: %d\r\n\r\n";
    
    NSString* msearchDatagram = [NSString stringWithFormat:datagramTemplate, MULTICAST_ADDRESS, MULTICAST_PORT, searchTarget, responseTime];
    
    return [msearchDatagram dataUsingEncoding:NSUTF8StringEncoding];
}


- (void)stopSearching {
    
    [self.discoverySocket close];
}


#pragma mark - socket delegate


- (void)asyncUdpSocket:(TSAsyncUdpSocket *)socket didReceiveData:(NSData *)data {
    
    TSSSDPResponse *response = [[TSSSDPResponse alloc] initWithData:data];

    [self callCompletionBlockWithSuccess:YES SSDPDictionary:response error:nil];
}


- (void)asyncUpdSocket:(TSAsyncUdpSocket *)socket didReceiveError:(NSError *)error {
    
    [self callCompletionBlockWithSuccess:NO SSDPDictionary:nil error:error];
}


#pragma mark - completion block

- (void)callCompletionBlockWithSuccess:(BOOL)success
                        SSDPDictionary:(TSSSDPResponse *)SSDPDictionary
                                 error:(NSError *)error {
    
    if (self.completionBlock) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
        
            self.completionBlock(success, SSDPDictionary, error);
        });
    }
}


@end
