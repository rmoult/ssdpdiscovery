//
//  Created by Richard Moult on 11/12/2014.
//  Copyright (c) 2014 TrickySquirrel. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface TSSSDPResponse : NSObject

@property (nonatomic, strong, readonly) NSDictionary *dictionary;
 
- (id)initWithData:(NSData *)data;

- (BOOL)doesDictionaryKey:(NSString *)key containString:(NSString *)server;

@end
