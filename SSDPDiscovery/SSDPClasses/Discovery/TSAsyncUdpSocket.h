//
//  Created by Richard Moult on 12/12/2014.
//  Copyright (c) 2014 TrickySquirrel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CocoaAsyncSocket/GCDAsyncUdpSocket.h>

extern NSString *const MULTICAST_ADDRESS;
extern NSInteger MULTICAST_PORT;


@class TSAsyncUdpSocket;


@protocol TSAsyncUdpSocketDelegate <NSObject>
- (void)asyncUdpSocket:(TSAsyncUdpSocket *)socket didReceiveData:(NSData *)data;
- (void)asyncUpdSocket:(TSAsyncUdpSocket *)socket didReceiveError:(NSError *)error;
@end


@interface TSAsyncUdpSocket : GCDAsyncUdpSocket

@property (nonatomic, weak) id<TSAsyncUdpSocketDelegate> socketDelegate;

- (id)initWithQueue:(dispatch_queue_t)queue;

- (void)addSSDPSocketFilter:(GCDAsyncUdpSocketReceiveFilterBlock)filter queue:(dispatch_queue_t)queue;

- (void)startBroadcastingWithData:(NSData *)data;

@end
