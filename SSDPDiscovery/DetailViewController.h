//
//  DetailViewController.h
//  SSDPDiscovery
//
//  Created by Richard Moult on 09/01/2015.
//  Copyright (c) 2015 TrickySquirrel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

@end

