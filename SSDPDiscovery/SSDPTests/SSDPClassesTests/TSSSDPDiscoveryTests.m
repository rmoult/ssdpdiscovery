//
//  Created by Richard Moult on 10/01/2015.
//  Copyright (c) 2015 TrickySquirrel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import <OCMock.h>
#import "TSSSDPDiscoveryTestHelpers.h"
#import "TSSSDPDiscovery.h"


@interface TSSSDPDiscoveryTests : XCTestCase
@property (nonatomic, strong) id mockSocket;
@property (nonatomic, strong) TSSSDPDiscovery *ssdpDiscovery;
@property (nonatomic, strong) TSSSDPDiscoverServiceBlock emptyCompletionBlock;
@end


@implementation TSSSDPDiscoveryTests

- (void)setUp {
    
    [super setUp];
    self.mockSocket = [OCMockObject niceMockForClass:[TSAsyncUdpSocket class]];
    self.ssdpDiscovery = [[TSSSDPDiscovery alloc] initWithAsyncUpdSocket:self.mockSocket];
    self.emptyCompletionBlock = ^(BOOL success, TSSSDPResponse *ssdpResponse, NSError *error) {};
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}


#pragma mark - init

- (void)testInit_throws {
    
    XCTAssertThrows([TSSSDPDiscovery new]);
}

- (void)testInit_returnsObject {
    
    id mockSocket = [OCMockObject niceMockForClass:[TSAsyncUdpSocket class]];
    XCTAssertNotNil([[TSSSDPDiscovery alloc] initWithAsyncUpdSocket:mockSocket]);
}


#pragma mark - start searching

- (void)testStartSearching_noCompletionBlock_throws {
    
    XCTAssertThrows([self.ssdpDiscovery startSearchingWithSearchTarget:@"" responseTime:3 completionBlock:nil]);
}

- (void)testStartSearching_noSearchTarget_throws {
    
    XCTAssertThrows([self.ssdpDiscovery startSearchingWithSearchTarget:nil responseTime:3 completionBlock:self.emptyCompletionBlock]);
}

- (void)testStartSearching_searchTarget_setCorrectData {
    
    NSString *searchTarget = @"target";
    NSData *expectedData = [TSSSDPDiscoveryTestHelpers multicastSearchDatagramWithSearchTarget:searchTarget responseTime:2];
    [[self.mockSocket expect] startBroadcastingWithData:expectedData];
    [self.ssdpDiscovery startSearchingWithSearchTarget:searchTarget responseTime:2 completionBlock:self.emptyCompletionBlock];
    XCTAssertNoThrow([self.mockSocket verify]);
}


#pragma mark - stop searching 

- (void)testStopSearching_callsCloseSocket {
    
    [[self.mockSocket expect] close];
    [self.ssdpDiscovery stopSearching];
    XCTAssertNoThrow([self.mockSocket verify]);
}


#pragma mark - socket delegate

-(void)testSocketDelgateDidReceiveData_callsCompletionBlockWithCorrectParameters {
    
    __block BOOL calledBlock = NO;

    XCTestExpectation *callBlockExpectation = [self expectationWithDescription:@"call completion block"];
    
    TSAsyncUdpSocket *discoverySocket = [TSSSDPDiscoveryTestHelpers updSocketWithStubbedStartBroadcastingFunctionality];
    TSSSDPDiscovery *ssdpDiscovery = [[TSSSDPDiscovery alloc] initWithAsyncUpdSocket:discoverySocket];
    
    TSSSDPDiscoverServiceBlock completionBlock = ^(BOOL success, TSSSDPResponse *ssdpResponse, NSError *error) {
        XCTAssertTrue(success);
        XCTAssertNotNil(ssdpResponse);
        XCTAssertNil(error);
        calledBlock = YES;
        [callBlockExpectation fulfill];
    };
    
    [ssdpDiscovery startSearchingWithSearchTarget:@"" responseTime:3 completionBlock:completionBlock];
 
    [discoverySocket.socketDelegate asyncUdpSocket:nil didReceiveData:[NSData new]];
    
    [self waitForExpectationsWithTimeout:1 handler:^(NSError *error) {
        XCTAssertTrue(calledBlock);
    }];
}


-(void)testSocketDelgateDidReceiveError_callsCompletionBlockWithCorrectParameters {
    
    __block BOOL calledBlock = NO;
    NSError *expectedError = [NSError new];
    
    XCTestExpectation *callBlockExpectation = [self expectationWithDescription:@"call completion block"];
    
    TSAsyncUdpSocket *discoverySocket = [TSSSDPDiscoveryTestHelpers updSocketWithStubbedStartBroadcastingFunctionality];
    TSSSDPDiscovery *ssdpDiscovery = [[TSSSDPDiscovery alloc] initWithAsyncUpdSocket:discoverySocket];
    
    TSSSDPDiscoverServiceBlock completionBlock = ^(BOOL success, TSSSDPResponse *ssdpResponse, NSError *error) {
        XCTAssertFalse(success);
        XCTAssertNil(ssdpResponse);
        XCTAssertEqualObjects(expectedError, error);
        calledBlock = YES;
        [callBlockExpectation fulfill];
    };
    
    [ssdpDiscovery startSearchingWithSearchTarget:@"" responseTime:3 completionBlock:completionBlock];
    
    [discoverySocket.socketDelegate asyncUpdSocket:nil didReceiveError:expectedError];
    
    [self waitForExpectationsWithTimeout:1 handler:^(NSError *error) {
        XCTAssertTrue(calledBlock);
    }];
}



@end
