//
//  Created by Richard Moult on 09/01/2015.
//  Copyright (c) 2015 TrickySquirrel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "TSSSDPDiscoveryTestHelpers.h"



@interface TSSSDPResponseTests : XCTestCase
@end



@implementation TSSSDPResponseTests


- (void)testInit_throws {
    
    XCTAssertThrows([TSSSDPResponse new]);
}

- (void)testInit_data_returnsObject {
    
    XCTAssertNotNil([[TSSSDPResponse alloc] initWithData:[NSData new]]);
}

- (void)testDoesKeyContainString_correctKeyValue_shouldReturnYES {
    
    TSSSDPResponse *response = [[TSSSDPResponse alloc] initWithData:[TSSSDPDiscoveryTestHelpers responseDataWithSearchTarget:TestRootDeviceSearchTarget]];
    XCTAssertTrue([response doesDictionaryKey:@"SERVER" containString:@"SKY"]);
}

- (void)testDoesKeyContainString_incorrectValueForKey_shouldReturnNO {
    
    TSSSDPResponse *response = [[TSSSDPResponse alloc] initWithData:[TSSSDPDiscoveryTestHelpers unknownResponseData]];
    XCTAssertFalse([response doesDictionaryKey:@"SERVER" containString:@"SKY"]);
}

- (void)testDoesKeyContainString_incorrectKey_shouldReturnNO {
    
    TSSSDPResponse *response = [[TSSSDPResponse alloc] initWithData:[TSSSDPDiscoveryTestHelpers unknownResponseData]];
    XCTAssertFalse([response doesDictionaryKey:@"BADKEY" containString:@"SKY"]);
}

- (void)testDictionaryMessage_returnsCorrectKeys {
    
    TSSSDPResponse *response = [[TSSSDPResponse alloc] initWithData:[TSSSDPDiscoveryTestHelpers responseDataWithSearchTarget:TestRootDeviceSearchTarget]];
    NSSet *expectedKeySet = [NSSet setWithArray:@[@"01-NLS", @"CACHE-CONTROL", @"DATE", @"LOCATION", @"OPT", @"SERVER", @"ST", @"USN", @"X-User-Agent"]];
    NSSet *returnedSet = [NSSet setWithArray:response.dictionary.allKeys];
    XCTAssertTrue([expectedKeySet isEqualToSet:returnedSet]);
}

- (void)testDictionaryMessage_returnsCorrectValues {
    
    TSSSDPResponse *response = [[TSSSDPResponse alloc] initWithData:[TSSSDPDiscoveryTestHelpers responseDataWithJustLocation]];
    NSSet *expectedKeySet = [NSSet setWithArray:@[@"Wed, 01 Jan 2014 00:50:56 GMT",@"max-age=115",@"http://192.168.5.103:49153/description1.xml"]];
    NSSet *returnedSet = [NSSet setWithArray:response.dictionary.allValues];
    XCTAssertTrue([expectedKeySet isEqualToSet:returnedSet]);
}


@end
