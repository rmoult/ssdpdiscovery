//
//  Created by Richard Moult on 14/01/2015.
//  Copyright (c) 2015 TrickySquirrel. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TSSSDPDiscovery.h"
#import "TSSSDPResponse.h"
#import "TSSSDPDiscovery.h"


#define TestRootDeviceSearchTarget @"upnp:rootdevice"



@interface TSSSDPDiscoveryTestHelpers : NSObject

+ (TSAsyncUdpSocket *)updSocketWithStubbedStartBroadcastingFunctionality;
    
+ (NSData *)multicastSearchDatagramWithSearchTarget:(NSString *)searchTarget responseTime:(NSInteger)responseTime;

+ (NSData *)unknownResponseData;

+ (NSData *)responseDataWithSearchTarget:(NSString *)searchTarget;

+ (NSData *)responseDataWithSearchTarget:(NSString *)searchTarget host:(NSString *)host;

+ (NSData *)responseDataNoLocationWithSearchTarget:(NSString *)searchTarget;
    
+ (NSData *)responseDataWithJustLocation;

@end

