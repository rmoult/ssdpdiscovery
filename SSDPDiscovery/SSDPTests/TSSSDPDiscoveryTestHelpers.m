//
//  Created by Richard Moult on 14/01/2015.
//  Copyright (c) 2015 TrickySquirrel. All rights reserved.
//

#import "TSSSDPDiscoveryTestHelpers.h"
#import <OCMock.h>

@implementation TSSSDPDiscoveryTestHelpers


+ (TSAsyncUdpSocket *)updSocketWithStubbedStartBroadcastingFunctionality {
    
    dispatch_queue_t searchQueue = dispatch_queue_create("com.yourdomain.upnp.ssdpDiscoveryQueue", NULL);
    TSAsyncUdpSocket *discoverySocket = [[TSAsyncUdpSocket alloc] initWithQueue:searchQueue];
    id mockSocket = [OCMockObject partialMockForObject:discoverySocket];
    [[[mockSocket stub] andDo:^(NSInvocation *invocation) {}] startBroadcastingWithData:OCMOCK_ANY];
    return discoverySocket;
}

+ (NSData *)multicastSearchDatagramWithSearchTarget:(NSString *)searchTarget responseTime:(NSInteger)responseTime {
    
    NSString *datagramTemplate = @"M-SEARCH * HTTP/1.1\r\nHOST: 239.255.255.250:1900\r\nST: %@\r\nMAN: \"ssdp:discover\"\r\nMX: %d\r\n\r\n";
    NSString* msearchDatagram = [NSString stringWithFormat:datagramTemplate, searchTarget, responseTime];
    return [msearchDatagram dataUsingEncoding:NSUTF8StringEncoding];
}


+ (NSData *)unknownResponseData {
    
    NSString *message = @"HTTP/1.1 200 OK\r\nCACHE-CONTROL: max-age=115\r\nDATE: Wed, 01 Jan 2014 00:50:56 GMT\r\nEXT:\r\nOPT: \"http://schemas.upnp.org/upnp/1/0/\"; ns=01\r\n01-NLS: 00e59e24-1dd2-11b2-9252-b9435af0a5bc\r\nSERVER: Linux/3.10.27 UPnP/1.0 BOB DLNADOC/1.50\r\nX-User-Agent: redsonic\r\nST: something else\r\nUSN: uuid:444D5276-3247-4761-7465-783e53ff5b90::urn:somethingelse";
    return [message dataUsingEncoding:NSUTF8StringEncoding];
}

+ (NSData *)responseDataWithSearchTarget:(NSString *)searchTarget host:(NSString *)host {
    
    NSString *message = [NSString stringWithFormat:@"HTTP/1.1 200 OK\r\nCACHE-CONTROL: max-age=115\r\nDATE: Wed, 01 Jan 2014 00:50:56 GMT\r\nEXT:\r\nLOCATION: http://%@:49153/description1.xml\r\nOPT: \"http://schemas.upnp.org/upnp/1/0/\"; ns=01\r\n01-NLS: 00e59e24-1dd2-11b2-9252-b9435af0a5bc\r\nSERVER: Linux/3.10.27 UPnP/1.0 SKY DLNADOC/1.50\r\nX-User-Agent: redsonic\r\nST: %@\r\nUSN: uuid:444D5276-3247-4761-7465-783e53ff5b90::urn:schemas-com:device:Sleeping:2", host, searchTarget];
    return [message dataUsingEncoding:NSUTF8StringEncoding];
}

+ (NSData *)responseDataWithSearchTarget:(NSString *)searchTarget {
    
    return [TSSSDPDiscoveryTestHelpers responseDataWithSearchTarget:searchTarget host:@"192.168.5.103"];
}

+ (NSData *)responseDataNoLocationWithSearchTarget:(NSString *)searchTarget {
    
    NSString *message = [NSString stringWithFormat:@"HTTP/1.1 200 OK\r\nCACHE-CONTROL: max-age=115\r\nDATE: Wed, 01 Jan 2014 00:50:56 GMT\r\nEXT:\r\nOPT: \"http://schemas.upnp.org/upnp/1/0/\"; ns=01\r\n01-NLS: 00e59e24-1dd2-11b2-9252-b9435af0a5bc\r\nSERVER: Linux/3.10.27 UPnP/1.0 SKY DLNADOC/1.50\r\nX-User-Agent: redsonic\r\nST: %@\r\nUSN: uuid:444D5276-3247-4761-7465-783e53ff5b90::urn:schemas-com:device:Sleeping:2", searchTarget];
    return [message dataUsingEncoding:NSUTF8StringEncoding];
}

+ (NSData *)responseDataWithJustLocation {
    
    NSString *message = @"HTTP/1.1 200 OK\r\nCACHE-CONTROL: max-age=115\r\nDATE: Wed, 01 Jan 2014 00:50:56 GMT\r\nEXT:\r\nLOCATION: http://192.168.5.103:49153/description1.xml";
    return [message dataUsingEncoding:NSUTF8StringEncoding];
}


@end
