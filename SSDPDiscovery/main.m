//
//  main.m
//  SSDPDiscovery
//
//  Created by Richard Moult on 09/01/2015.
//  Copyright (c) 2015 TrickySquirrel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
