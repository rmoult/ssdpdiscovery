//
//  AppDelegate.h
//  SSDPDiscovery
//
//  Created by Richard Moult on 09/01/2015.
//  Copyright (c) 2015 TrickySquirrel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

