//
//  Created by Richard Moult on 09/01/2015.
//  Copyright (c) 2015 TrickySquirrel. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"
#import "TSSSDPDiscovery.h"


@interface MasterViewController ()
@property (nonatomic, strong) NSMutableArray *objects;
@property (nonatomic, strong) dispatch_queue_t searchQueue;
@property (nonatomic, strong) TSAsyncUdpSocket *discoverySocket;
@property (nonatomic, strong) TSSSDPDiscovery *discoverDevice;
@end



@implementation MasterViewController

- (void)awakeFromNib {
    
    [super awakeFromNib];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        self.clearsSelectionOnViewWillAppear = NO;
        self.preferredContentSize = CGSizeMake(320.0, 600.0);
    }
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(discoverServices:)];
    
    self.navigationItem.leftBarButtonItem = addButton;

    self.detailViewController = (DetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
}


#pragma mark - SSDP Discoverey

- (void)discoverServices:(id)sender {
    
    [self resetData];
    
    [self stopSearching];
    
    [self startSearchingForDevices];
}


- (void)startSearchingForDevices {
    
    self.discoverDevice = [self createSSDPDiscoveryDevice];
    
    __weak typeof(self) weakSelf = self;
    
    [self.discoverDevice startSearchingWithSearchTarget:@"upnp:rootdevice" responseTime:3 completionBlock:^(BOOL success, TSSSDPResponse *ssdpResponse, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [weakSelf.objects addObject:[ssdpResponse.dictionary copy]];
            [weakSelf.tableView reloadData];
        });
    }];
}


- (TSSSDPDiscovery *)createSSDPDiscoveryDevice {
    
    self.searchQueue = dispatch_queue_create("com.yourdomain.upnp.ssdpDiscoveryQueue", NULL);
    
    self.discoverySocket = [[TSAsyncUdpSocket alloc] initWithQueue:self.searchQueue];
    
    [self.discoverySocket addSSDPSocketFilter:[self SSDPSocketDummyFilter] queue:dispatch_get_main_queue()];
    
    return [[TSSSDPDiscovery alloc] initWithAsyncUpdSocket:self.discoverySocket];
}


- (GCDAsyncUdpSocketReceiveFilterBlock)SSDPSocketDummyFilter {
    
    GCDAsyncUdpSocketReceiveFilterBlock filterBlock = ^BOOL(NSData *data, NSData *address, id *context) {
        
        BOOL filterPassed = NO;
        
        // check something
        
        filterPassed = YES;
        
        return filterPassed;
    };
    
    return filterBlock;
}


- (void)stopSearching {
    
    if (self.discoverDevice) {
        
        [self.discoverDevice stopSearching];
    }
}


#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSDictionary *object = self.objects[indexPath.row];
        DetailViewController *controller = (DetailViewController *)[[segue destinationViewController] topViewController];
        [controller setDetailItem:object];
        controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        controller.navigationItem.leftItemsSupplementBackButton = YES;
    }
}


#pragma mark - Reset Table View

- (void)resetData {
    
    self.objects = [NSMutableArray new];
    
    [self.tableView reloadData];
}


#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];

    cell.textLabel.text = [@(indexPath.row) stringValue];
    return cell;
}


@end
