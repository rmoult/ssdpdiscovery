# SSDPDiscovery
SSDPDiscovery gives easy access to using M-SEARCH to discover devices.  To run the project please first run pod install, or if you want to just use the project as a pod use the instructions below.


# Pod Install

```
pod 'SSDPDiscovery', :git => 'https://bitbucket.org/rmoult/ssdpdiscovery'
```

# Example SSDPDiscovery


Properties

```
@property (nonatomic, strong) dispatch_queue_t searchQueue;
@property (nonatomic, strong) TSAsyncUdpSocket *discoverySocket;
@property (nonatomic, strong) TSSSDPDiscovery *discoverDevice;
```

Functions

```
- (void)startSearchingForDevices {

    self.discoverDevice = [self createSSDPDiscoveryDevice];

    [self.discoverDevice startSearchingWithSearchTarget:@"upnp:rootdevice" completionBlock:^(BOOL success, TSSSDPResponse *ssdpResponse, NSError *error) {

        NSLog(@"%@", ssdpResponse.dictionary);
    }];
}


- (TSSSDPDiscovery *)createSSDPDiscoveryDevice {

    self.searchQueue = dispatch_queue_create("com.yourdomain.upnp.ssdpDiscoveryQueue", NULL);

    self.discoverySocket = [[TSAsyncUdpSocket alloc] initWithQueue:self.searchQueue];

    return [[TSSSDPDiscovery alloc] initWithAsyncUpdSocket:self.discoverySocket];
}
```

Socket Filters


```
[self.discoverySocket addSSDPSocketFilter:filter queue:dispatch_get_main_queue()];


+ (GCDAsyncUdpSocketReceiveFilterBlock)SSDPSocketFilterKey:(NSString *)key containingString:(NSString *)string {

    GCDAsyncUdpSocketReceiveFilterBlock filterBlock = ^BOOL(NSData *data, NSData *address, id *context) {

        BOOL filterPassed = NO;

        // check data and set filterPassed

        return filterPassed;
    };

    return filterBlock;
}
```
